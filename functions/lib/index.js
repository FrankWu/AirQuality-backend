"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const firestore_1 = require("@google-cloud/firestore");
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
admin.initializeApp();
const firestore = new firestore_1.Firestore();
const settings = { /* your settings... */ timestampsInSnapshots: true };
firestore.settings(settings);
exports.helloWorld = functions.https.onRequest((request, response) => {
    console.log("helloWorld function triggle");
    response.send("Hello from AirQualityApp!");
});
exports.getAirboxes = functions.https.onRequest((request, response) => {
    console.log("getAirboxes triggle");
    const coordinate = request.query["coordinate"];
    const radius = request.query["radius"];
    const userPoint = getGeoPoint(coordinate);
    const promise = admin.firestore().collection('airboxes').get();
    const p2 = promise.then(querySnapshot => {
        let jsonDoc = [];
        const data = querySnapshot.docs.forEach(doc => {
            const point = doc.data()['coordinate'];
            if (measure(userPoint, point) < radius) {
                jsonDoc.push(doc.data());
            }
        });
        console.log(`result:${jsonDoc.length}`);
        response.send(jsonDoc);
    });
    p2.catch(error => {
        console.log(error);
        response.status(500).send(error);
    });
});
function getGeoPoint(parameter) {
    const seperate = parameter.indexOf(',');
    const lat = Number(parameter.substr(0, seperate));
    const lon = Number(parameter.substring(seperate + 1, parameter.length));
    return new firestore_1.GeoPoint(lat, lon);
}
function measure(coordinate1, coordinate2) {
    const R = 6378.137; // Radius of earth in KM
    var dLat = (coordinate2.latitude - coordinate1.latitude) * Math.PI / 180;
    var dLon = (coordinate2.longitude - coordinate1.longitude) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(coordinate1.latitude * Math.PI / 180) * Math.cos(coordinate2.latitude * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d * 1000;
}
//# sourceMappingURL=index.js.map